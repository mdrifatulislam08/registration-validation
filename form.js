const form = document.getElementById('signup');
        const username = document.getElementById('username');
        const email = document.getElementById('email');
        const number = document.getElementById('number');
        const password = document.getElementById('password');
        const cpassword = document.getElementById('cpassword');

        //add event
        form.addEventListener('submit', (event) => {
            event.preventDefault();
            validate();
        })
                                        
        const sendData = (usernameVal,sRate, count) => {
    
            if(sRate === count){
                //alert('registration successful');
                swal("Welcome! "+usernameVal, "registration successful", "success");
            }

        }

        //For final data validation 
        const successMsg = (usernameVal) => {
            let formCont = document.getElementsByClassName('form-control');
            var count = formCont.length - 1;
            for(var i = 0; i < formCont.length; i++){
                if(formCont[i].className === "form-control success"){
                    var sRate = 0 + i;
                    //
                    sendData(usernameVal,sRate,count);  
                }else {
                    return false;
                }
            }
        }

        //more email validate

        const isEmail = (emailVal) => {
            var atSymbol = emailVal.indexOf("@");
            if(atSymbol < 1) return false;
            var dot = emailVal.lastIndexOf('.');
            if(dot <= atSymbol + 2) return false;
            if(dot === emailVal.length - 1 ) return false;
            return true;
        }

        //define the validate function
        const validate = () => {
        const usernameVal = username.value.trim();
        const emailVal = email.value.trim();
        const numberVal = number.value.trim();
        const passwordVal = password.value.trim();
        const cpasswordVal = cpassword.value.trim();
        
        //validate username
        if(usernameVal === ""){
            setErrorMsg(username, 'username cannot be blank');
        }else if(usernameVal.length <= 2){
            setErrorMsg(username, 'username minimum 3 character');
        }else{
            setSuccessMsg(username);
        }

        if(emailVal === ""){ 
            setErrorMsg(email, 'email cannot be blank');
        }else if(!isEmail(emailVal)){
            setErrorMsg(email, 'Not a valid email');
        }else{
            setSuccessMsg(email);
        }

        if(numberVal === ""){ 
            setErrorMsg(number, 'number cannot be blank');
        }else if(numberVal.length <= 10){
            setErrorMsg(number, 'Not a valid number');
        }else{
            setSuccessMsg(number);
        }

        if(passwordVal === ""){ 
            setErrorMsg(password, 'number cannot be blank');
        }else if(passwordVal.length <= 5){
            setErrorMsg(password, 'minimum 6 character');
        }else{
            setSuccessMsg(password);
        }

        if(cpasswordVal === ""){ 
            setErrorMsg(cpassword, 'confirm password cannot be blank');
        }else if(passwordVal != cpasswordVal){
            setErrorMsg(cpassword, 'confirm are not match');
        }else{
            setSuccessMsg(cpassword);
        }

        successMsg(usernameVal);
    }
        function setErrorMsg(input, errormsgs){
            const formControl = input.parentElement;
            const small = formControl.querySelector('small');
            formControl.className = "form-control error";
            small.innerText = errormsgs;
        }

        function setSuccessMsg(input){
            const formControl = input.parentElement;
            formControl.className = "form-control success";
        }