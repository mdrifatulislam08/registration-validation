<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <?php include 'links/links.php'?>
    
</head>
<body>
    <div class="container">
        <div class="header">
            <h2>Registration Form</h2>
        </div>
        <form action="" class="form" id="signup">
            <div class="form-control">
                <label for="">Username</label>
                <input type="text" placeholder="Enter your full name" id="username" autocomplete="off">
                <i class="fas fa-check-circle"></i>
                <i class="fas fa-exclamation-circle"></i>
                <small>Error msg</small>
            </div>
            <div class="form-control">
                <label for="">Email</label>
                <input type="email" placeholder="Enter your email" id="email" autocomplete="off">
                <i class="fas fa-check-circle"></i>
                <i class="fas fa-exclamation-circle"></i>
                <small>Error msg</small>
            </div>
            <div class="form-control">
                <label for="">Phone Number</label>
                <input type="number" placeholder="Enter your phone number" id="number" autocomplete="off">
                <i class="fas fa-check-circle"></i>
                <i class="fas fa-exclamation-circle"></i>
                <small>Error msg</small>
            </div>
            <div class="form-control">
                <label for="">password</label>
                <input type="password" placeholder="Enter your password" id="password" autocomplete="off">
                <i class="fas fa-check-circle"></i>
                <i class="fas fa-exclamation-circle"></i>
                <small>Error msg</small>
            </div>
            <div class="form-control">
                <label for="">Confirm password</label>
                <input type="password" placeholder="Enter your password again" id="cpassword" autocomplete="off">
                <i class="fas fa-check-circle"></i>
                <i class="fas fa-exclamation-circle"></i>
                <small>Error msg</small>
            </div>
            <input type="submit" class="btn" value="submit">
        </form>
    </div>


    <!-- ---------------javascript links---------------- -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="form.js"></script>
</body>
</html>